import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Index</title>
        <link rel="icon" href="/favicon.ico" />
        <meta property="og:title" content="National Basketball Association (NBA)" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="google.com" />
        <meta property="og:image" content="https://cdn.cnn.com/cnnnext/dam/assets/190919130022-20190918-trump-tax-forms-generic-large-tease.jpg" />
        <meta property="og:description" content="THE National Basketball Association (NBA) and Smart Communications last week relaunched the leagues website here aimed at providing content that suits local fans taste. In a virtual press conference on Thursday, officials of NBA Asia and Smart ..." />
        <meta property="og:site_name" content="Media Meter, Inc." />
      </Head>

      <h1>Welcome to Next</h1>
    </div>
  )
}
