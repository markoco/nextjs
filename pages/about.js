import Head from "next/head";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>About</title>
        <link rel="icon" href="/favicon.ico" />
        <meta property="og:title" content="Abut Next title OG Tag" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="goole.com" />
        <meta property="og:image" content="http://www.media-meter.net.s3-ap-southeast-1.amazonaws.com/uploads/images/mediameter_logo_20200218.png" />
        <meta property="og:description" content="About This is just a sample" />
        <meta property="og:site_name" content="Media Meter, Inc." />
      </Head>

      <h1>About Next</h1>
    </div>
  );
}
